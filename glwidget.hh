#ifndef GLWIDGET_HH
#define GLWIDGET_HH

# include <QGLWidget>
# include <QtOpenGL>
# include <sharedbuffer.hh>

# ifdef __unix__
#  include <GL/glu.h>
# elif defined __APPLE__
#  include <OpenGL/glu.h>
# endif


class GLWidget : public QGLWidget
{
  Q_OBJECT;

public:
  GLWidget(QWidget *parent=0);

  typedef struct
  {
      float x;
      float y;
  } s_cube_pos;

  void set_input_buffer(SharedBuffer<s_cube_pos>* buffer);

protected:
  void initializeGL();
  void resizeGL(int w, int h);
  void paintGL();
  void draw_floor();
  void draw_cube();

signals:
  void fpsUpdate(int fps);

public slots:
  void timeOut();

private:
  QTime timer;
  int time_before;
  int etime;
  float x, y, z;
  float pos_x;
  float pos_y;
  SharedBuffer<s_cube_pos>* buffer_m;
};

#endif // GLWIDGET_HH
