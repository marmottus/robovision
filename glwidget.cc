#include <cmath>
#include "glwidget.hh"

GLWidget::GLWidget(QWidget *parent)
    : QGLWidget(parent), x(0), y(0), z(0), pos_x(0), pos_y(0),
      buffer_m(NULL)
{
  timer.start();
  time_before = timer.elapsed();
  etime = 0;
}

void GLWidget::initializeGL()
{
  glShadeModel(GL_SMOOTH);
  glClearColor(0, 0, 0, 1);
  glEnable(GL_DEPTH_TEST);
  glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

  // Light
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_COLOR_MATERIAL);
    GLfloat light_pos[4] = {-1, 0, 1, 1};
    glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

    GLfloat light_ambient[4] = {0.0, 0.0, 0.0, 1};
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);

    GLfloat light_diffuse[4] = {1, 1, 1, 1};
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

    // Texture
    GLuint floor_tex;
    glGenTextures(1, &floor_tex);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glBindTexture(GL_TEXTURE_2D, floor_tex);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

#define SIZE 32
    GLubyte tex_data[SIZE][SIZE][3];

    for (int i = 0; i < SIZE; i++)
    {
          for (int j = 0; j < SIZE; j++)
          {
             GLubyte c = ((!(i&16) ^ !(j&16))) * 255;
             tex_data[i][j][0] = c;
             tex_data[i][j][1] = c;
             tex_data[i][j][2] = c;
          }
       }

    gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB,
                 SIZE, SIZE, GL_RGB, GL_UNSIGNED_BYTE, tex_data);
}

void GLWidget::resizeGL(int width,int height)
{
// Prevent A Divide By Zero
  if (height == 0)
    height= 1 ;

  glViewport(0, 0, width,height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(45.0f, (GLfloat)width/(GLfloat)height, 0.1f, 100.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void GLWidget::draw_floor()
{
    glColor3f(1, 1, 1);
    glBegin(GL_QUADS);
    {
      glTexCoord2f(0, 100);
      glVertex3f(-100,  100, 0);
      glTexCoord2f(100, 100);
      glVertex3f( 100,  100, 0);
      glTexCoord2f(100, 0);
      glVertex3f( 100, -100, 0);
      glTexCoord2f(0, 0);
      glVertex3f(-100, -100, 0);
    }
    glEnd();
}

void GLWidget::draw_cube()
{
    glColor3f(1, 0, 0);
    glBegin(GL_QUADS);
    {
      glNormal3f(0, 0, 1);
      glVertex3f(-1,  1, 1);
      glVertex3f( 1,  1, 1);
      glVertex3f( 1, -1, 1);
      glVertex3f(-1, -1, 1);

      glNormal3f(-1, 0, 0);
      glVertex3f(-1, -1,  1);
      glVertex3f(-1, 1,  1);
      glVertex3f(-1, 1, -1);
      glVertex3f(-1, -1, -1);

      glNormal3f(1, 0, 0);
      glVertex3f(1, -1,  1);
      glVertex3f(1, 1,  1);
      glVertex3f(1, 1, -1);
      glVertex3f(1, -1, -1);

      glNormal3f(0, -1, 0);
      glVertex3f(-1, -1, 1);
      glVertex3f( 1, -1, 1);
      glVertex3f( 1, -1,-1);
      glVertex3f(-1, -1,-1);

       glNormal3f(0, 1, 0);
      glVertex3f(-1, 1, 1);
      glVertex3f( 1, 1, 1);
      glVertex3f( 1, 1,-1);
      glVertex3f(-1, 1,-1);
    }
    glEnd();
}

void GLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    gluLookAt(0, 0, 10, 0, 0, 0, 0, -1, 0);

    glEnable(GL_TEXTURE_2D);
    draw_floor();
    glDisable(GL_TEXTURE_2D);
    glTranslatef(x, y, z);
    draw_cube();
}

void GLWidget::timeOut()
{
    s_cube_pos pos;
    if (buffer_m && buffer_m->dataAvailable())
    {
        pos = buffer_m->get();
        pos_x = pos.x-0.5;
        pos_y = pos.y-0.5;
    }

  int current_time = timer.elapsed();
  etime = current_time - time_before;
  time_before = current_time;
  emit fpsUpdate(1000/(etime?etime:1));

    x -= pos_x * etime / 1000.0 * 2.0;
    y += pos_y * etime / 1000.0 * 2.0;

  updateGL();
}

void GLWidget::set_input_buffer(SharedBuffer<s_cube_pos> *buffer)
{
    buffer_m = buffer;
}
