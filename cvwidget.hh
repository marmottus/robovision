#ifndef CVWIDGET_HH
#define CVWIDGET_HH

# include <QThread>
# include <QMutex>
# include <QQueue>
# include <QTime>
# include <QColor>
# include <QVector>
# include <QRect>

# include <opencv/cv.h>
# include <opencv/highgui.h>

# include <sharedbuffer.hh>
# include <glwidget.hh>


class CVWidget : public QThread
{
    Q_OBJECT

    // PUBLIC
public:
    explicit CVWidget(QObject *parent = 0);
    ~CVWidget();
    void setBuffer(SharedBuffer<cv::Mat>* buffer);
    void setGLBuffer(SharedBuffer<GLWidget::s_cube_pos>* buffer);
    void setRectBuffer(SharedBuffer<cv::Rect> *buffer);

public slots:
    void openVideo();
    void openDevice();
    void play();
    void stop();
    void close();
    void setPos(int);
    void toggleOutput();
    void changeColor(int x, int y);
    void changeTolerance(int t);
    void changeMatch(bool b);
    void changeThreshold(int v);

    void setRect(int x, int y, int w, int h);
    void setROI(bool b);

    // PRIVATE
private:
    enum VideoType
    {
        DEVICE,
        FILE
    }                   videoType_m;

    enum OutputType
    {
        VIDEO,
        MASK
    }                   outputType_m;

    QString             file_m;
    QMutex              mutex_m;
    cv::VideoCapture    in_m;
    bool                startFlag_m;
    QTime               timer_m;
    int                 time_m;
    SharedBuffer<cv::Mat>*              buffer_m;
    SharedBuffer<GLWidget::s_cube_pos>* gl_buffer_m;
    SharedBuffer<cv::Rect>*             rect_buffer_m;
    cv::Mat             frame_m;

    int                 h_m;
    int                 s_m;
    int                 v_m;
    int                 tolerance_m;
    int                 threshold_m;
    int                 n_pixels_m;
    cv::Point           pos_m;
    QColor              color_m;
    bool                show_match_m;
    QVector<cv::Point>  neighbors_m;
    QList<cv::Point>    cloud_m;
    cv::Rect            rect_m;
    bool                use_roi_m;
    cv::Size            size_m;

    cv::Point binarize();
    void run();
    void open();
    void openFile();
    void labelize(cv::Mat& mask);
    void neighbors(int x, int y);
    QColor random_color();
    int distance(cv::Point const& p1, cv::Point const& p2);
    void checkRect();
    void setRectPos(int x, int y);
signals:
    void totalFrame(double);
    void fpsUpdate(int fps);
    void opened();
    void closed();
    void stopped();
    void newFrame();
    void played();
    void newPos(int);
    void colorChanged(QColor);
    void rectSize(int x, int y, int rows, int cols);
};

# include <cvwidget.hxx>

#endif // CVWIDGET_HH
