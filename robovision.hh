#ifndef ROBOVISION_HH
#define ROBOVISION_HH

# include <QMainWindow>
# include <QTimer>

# include <ui_robovision.h>
# include <process.hh>
# include <glwidget.hh>
# include <video_widget.hh>
# include <sharedbuffer.hh>

namespace Ui {
class roboVision;
}

class roboVision : public QMainWindow
{
    Q_OBJECT

public:
    explicit roboVision(QWidget *parent = 0);
    ~roboVision();

private slots:
    void video_open();
    void video_close();
    void video_stop();
    void video_play();
    void set_total_frames(double n);
    void video_set_pos(int pos);
    void set_object_color(QColor);
    void set_ROI_infos(int x, int y, int w, int h);
    void set_smooth_height(int h);
    void set_smooth_width(int w);
    void change_filter_type(int index);
    void set_cv_timer_interval(double interval);

private:
    void connections();

    Ui::roboVision*             ui_m;
    VideoWidget*                video_widget_m;
    Process*                    process_m;
    SharedBuffer<cv::Mat>*                    input_video_buffer_m;
    SharedBuffer<cv::Mat>*                    output_video_buffer_m;
    SharedBuffer<GLWidget::s_cube_pos>*       gl_buffer_m;
    SharedBuffer<cv::Rect>*                   rect_input_buffer_m;
    SharedBuffer<cv::Rect>*                   rect_output_buffer_m;
    QTimer                      *cv_timer_m;
    QTimer                      *gl_timer_m;
};

# include <robovision.hxx>

#endif // ROBOVISION_HH
