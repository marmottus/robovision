#ifndef VIDEOWIDGET_HH
#define VIDEOWIDGET_HH

# include <QObject>


# include <video_thread.hh>

class VideoWidget : public QObject
{
  Q_OBJECT
public:
  explicit VideoWidget(QObject *parent = 0);
  void set_output_buffer(SharedBuffer<cv::Mat>* buffer);
  ~VideoWidget();

signals:
  void pos_updated(int pos);
  void video_fps_updated(double fps);
  void fps_updated(int fps);
  void total_frames_updated(double nframes);

  void opened();
  void closed();
  void stopped();
  void stopping(bool b);
  void started();
  void starting(bool b);

public slots:

  void start();
  void stop();
  void close();

  void open();
  void open_file();
  void open_device();

  void set_pos(int pos);

private:
  VideoThread               thread_m;
  SharedBuffer<cv::Mat>*    buffer_m;
};

#endif // VIDEOWIDGET_HH
