#ifndef CV_GL_WIDGET_HXX
# define CV_GL_WIDGET_HXX

# include <cv_gl_widget.hh>

inline void CVGLWidget::set_input_buffer(SharedBuffer<cv::Mat> *buffer)
{
    video_buffer_m = buffer;
}

inline void CVGLWidget::set_rect_input_buffer(SharedBuffer<cv::Rect>* buffer)
{
  rect_input_buffer_m = buffer;
}

inline void CVGLWidget::set_rect_output_buffer(SharedBuffer<cv::Rect>* buffer)
{
  rect_output_buffer_m = buffer;
}

inline void CVGLWidget::set_ROI(bool b)
{
  mutex_m.lock();
  {
    use_roi_m = b;
  }
  mutex_m.unlock();
}


inline void CVGLWidget::update_image_size_and_click()
{
    QPoint  p = mouse_pos_m;
    int iw = image_m.width();
    int ih = image_m.height();
    int ix = ((p.x() - image_pos_m.x()) * iw) / (width() - image_pos_m.x()*2);
    int iy = (ih) - (((p.y() - image_pos_m.y()) * ih)
                     / (height() - image_pos_m.y()*2));

    if (ix < 0)
        ix = 0;
    if (iy < 0)
        iy = 0;
    if (ix >= iw)
        ix = iw-1;
    if (iy >= ih)
        iy = ih-1;

    image_rect_m = QRect(ix, iy, iw, ih);
}

inline void CVGLWidget::paint()
{
    if (!use_roi_m)
        return;


    if (abs(select_rect_m.width()) >= 1
            && abs(select_rect_m.height()) >= 1)
    {
        QPainter painter;
        painter.begin(&image_m);
        {
            if (selected_m && !just_click_m)
                painter.setBrush(QColor(0, 0, 255, 50));
            else
                painter.setBrush(QBrush(Qt::NoBrush));

            painter.setPen(QPen(QColor(Qt::red)));
            painter.drawRect(select_rect_m);
        }
        painter.end();
    }
}

inline void CVGLWidget::check_rect()
{
    if (select_rect_m.width() < 0)
    {
        if (select_rect_m.right() < 0)
            select_rect_m.moveTo(-select_rect_m.width(), select_rect_m.y());
        if (select_rect_m.left() >= image_m.width())
            select_rect_m.moveTo(image_m.width()-1, select_rect_m.y());
    }
    else
    {
        if (select_rect_m.left() < 0)
            select_rect_m.moveTo(0, select_rect_m.y());
        if (select_rect_m.right() >= image_m.width())
            select_rect_m.moveTo(image_m.width() - select_rect_m.width() - 1,
                                 select_rect_m.y());
    }

    if (select_rect_m.height() < 0)
    {
        if (select_rect_m.top() >= image_m.height())
            select_rect_m.moveTo(select_rect_m.x(), image_m.height()-1);
        if (select_rect_m.bottom() < 0)
            select_rect_m.moveTo(select_rect_m.x(),
                                 -select_rect_m.height());
    }
    else
    {
        if (select_rect_m.top() < 0)
            select_rect_m.moveTo(select_rect_m.x(), 0);
        if (select_rect_m.bottom() >= image_m.height())
            select_rect_m.moveTo(select_rect_m.x(),
                                 image_m.height() - select_rect_m.height() - 1);
    }
}

inline void CVGLWidget::move()
{
    QRect ri = image_rect_m;

    if (abs(ri.x() - select_rect_m.left()) < abs(ri.x() - select_rect_m.right()))
        select_rect_m.setLeft(ri.x());
    else
        select_rect_m.setRight(ri.x());

    if (abs(ri.y() - select_rect_m.top()) < abs(ri.y() - select_rect_m.bottom()))
        select_rect_m.setTop(ri.y());
    else
        select_rect_m.setBottom(ri.y());
}

inline void CVGLWidget::set_rect(const cv::Rect& rect)
{
  QRect ri = image_rect_m;

  int sx = (rect.x * ri.width()) / frame_m.cols;
  int sw = (rect.width * ri.width()) / frame_m.cols;
  int sh = (rect.height * ri.height()) / frame_m.rows;
  int sy = ri.height()  - ((rect.y * ri.height()) / frame_m.rows);

  select_rect_m = QRect(sx, sy, sw, -sh);
}

inline void CVGLWidget::release()
{
    QRect ri = image_rect_m;

    selected_m = false;

    int dx = ri.x() - click_pos_m.x();
    int dy = ri.y() - click_pos_m.y();

    if (abs(dx) < 3 && abs(dy) < 3)
    {
        int sx = (click_pos_m.x() * frame_m.cols) / ri.width();
        int sy = ((ri.height()-click_pos_m.y()) * frame_m.rows) / ri.height();
        emit clickColor(sx, sy);
    }
    else
    {
        int lx = select_rect_m.x();
        int ly = ri.height() - select_rect_m.y();

        if (select_rect_m.width() < 0)
            lx += select_rect_m.width();

        if (select_rect_m.height() > 0)
            ly -= select_rect_m.height();

        QRect selection(lx, ly,
                        abs(select_rect_m.width()),
                        abs(select_rect_m.height()));

        int sw = (selection.width() * frame_m.cols) / ri.width();
        int sh = (selection.height() * frame_m.rows) / ri.height();
        int sx = (selection.x() * frame_m.cols) / ri.width();
        int sy = (selection.y() * frame_m.rows) / ri.height();

        if (rect_output_buffer_m && use_roi_m)
          rect_output_buffer_m->add(cv::Rect(sx, sy, sw, sh));
    }
}



#endif // CV_GL_WIDGET_HXX
