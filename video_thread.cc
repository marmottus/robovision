#include <video_thread.hh>

#include <QFileDialog>
#include <QStack>
#include <QDebug>

VideoThread::VideoThread(QObject *parent)
  : QThread(parent),
    startFlag_m(false),
    buffer_m(NULL)

{
  timer_m.start();
  time_m = timer_m.elapsed();
}

VideoThread::~VideoThread()
{
}

void VideoThread::run()
{
  int current_time;
  double delta;

  while (startFlag_m)
  {
    current_time = timer_m.elapsed();
    delta = current_time - time_m;

    if (delta >= 1000.0f/video_fps_m || videoType_m == DEVICE)
    {
      emit fps_updated(1000/delta);

      mutex_m.lock();
      {
        if (videoType_m == FILE)
        {
          in_m.set(CV_CAP_PROP_POS_FRAMES, ++current_frame_m);
          in_m.retrieve(frame_m);
        }
        else
          in_m >> frame_m;

        if (frame_m.cols > 0 && frame_m.rows > 0)
        {
          if (buffer_m)
            buffer_m->add(frame_m.clone());

          if (videoType_m == FILE)
            emit pos_updated(current_frame_m);
        }

        if (videoType_m == FILE && current_frame_m >= total_frame_m)
          current_frame_m = -1;

        time_m = current_time;
      }
      mutex_m.unlock();
    }
    else
      msleep(1000/video_fps_m - delta);
  }
}

bool VideoThread::play()
{
  bool ok = !isRunning();
  mutex_m.lock();
  {
    if (ok)
      startFlag_m = true;
  }
  mutex_m.unlock();

  if (ok)
    start();

  return ok;
}

void VideoThread::close()
{
  mutex_m.lock();
  {
    if (in_m.isOpened())
      in_m.release();
  }
  mutex_m.unlock();
}

void VideoThread::stop()
{
  mutex_m.lock();
  {
    startFlag_m = false;
  }
  mutex_m.unlock();
}

bool VideoThread::open_device()
{
  bool ok = false;
  mutex_m.lock();
  {
    in_m.open(0);
    videoType_m = DEVICE;
    ok = in_m.isOpened();
  }
  mutex_m.unlock();

  return ok;
}

bool VideoThread::open_file()
{
  file_m = QFileDialog::getOpenFileName(NULL,
                                        tr("Open File"),
                                        "/home",
                                        tr("Videos (*.avi *.wmv *.mp4 *.webm *.flv)"));

  bool ok = false;

  if (!file_m.isEmpty())
  {
    mutex_m.lock();
    {
      in_m.open(file_m.toStdString());
      videoType_m = FILE;
      ok = in_m.isOpened();
    }
    mutex_m.unlock();
  }

  return ok;
}

void VideoThread::open()
{
  mutex_m.lock();
  {
    if (videoType_m == FILE)
    {
      current_frame_m = 0;
      in_m.set(CV_CAP_PROP_POS_FRAMES, 0);
      in_m.retrieve(frame_m);
    }
    else
      in_m >> frame_m;

    if (buffer_m)
      buffer_m->add(frame_m.clone());

    video_fps_m = in_m.get(CV_CAP_PROP_FPS);

    if (video_fps_m == -1)
      video_fps_m = 30;
  }
  mutex_m.unlock();

  total_frame_m = in_m.get(CV_CAP_PROP_FRAME_COUNT);
}
